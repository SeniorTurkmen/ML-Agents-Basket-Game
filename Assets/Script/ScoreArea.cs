﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreArea : MonoBehaviour
{
    //Score Text view on Score Board
    //public Text scoreText;
    public bool bsktScore = false;
    //Score Variable
    public int Score = 0;

    //Ball is Basket Controller
    private void OnTriggerEnter(Collider other) {
        var checker = FindObjectOfType<Checker>(); // Calling Checker Script
        if(checker != null)// Controlled Checker script
        {
            if (checker.aBc)
            {
                Score++;          //Score is Augmenting
                bsktScore = true;
                checker.aBc = false;
            }

        }

        //scoreText.text = Score.ToString();  //Score Text is Writing ScoreBoard
    }

}
