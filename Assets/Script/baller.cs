﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class baller : MonoBehaviour
{
    Vector2 startPos, endPos, direction;
    private Vector3 Controller,temp, throwForce;
    Rigidbody rb;

    public bool endCheck = false;
    float touchTimeStart, touchTimeFinish, timeInterval;

    [SerializeField]
    float throwForceInXandY= 40f;
    [SerializeField]
    float throwForceInZ= 500f;
    [SerializeField]
    private Transform _ballTarget;
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Camera cam;
    [SerializeField]
    private float gravity = -100;

    private bool _inProgress;
    private Vector3 direction1;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Physics.gravity = new Vector3(0,gravity,0);
    }

    private void Update()
    {
        if(!_inProgress)
        {
            ProcessInput();
        }
    }

    private void ProcessInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchTimeStart = Time.time;
            startPos = Input.mousePosition;
        }

        if(Input.GetMouseButtonUp(0))
        {
            _inProgress = true;
            touchTimeFinish = Time.time;
            timeInterval = touchTimeFinish - touchTimeStart;
            endPos = Input.mousePosition;
            direction = startPos - endPos;
            rb.isKinematic = false;
            throwForce = new Vector3(-direction.x * throwForceInXandY , -direction.y * throwForceInXandY , throwForceInZ/timeInterval+2f);
            rb.AddForce(throwForce);
            Invoke("ReturnBall", 5f);
        }
    }

    public void ReturnBall()
    {
        _inProgress = false;
        rb.isKinematic = true;
        Born();
        transform.position = Controller;
        SetCameraLocation();

        rb.velocity = Vector3.zero;
        var checker = FindObjectOfType<Checker>(); // Calling Checker Script
        var score = FindObjectOfType<ScoreArea>(); // Calling Checker Script
        checker.aBc = false;
        score.bsktScore = false;
        endCheck = false;
    }

    private void Born()
    {
        temp = new Vector3(Random.value*500/3,5,Random.value*500/3);

        if(((50f < temp.x && temp.z > 89f) && (99f > temp.x && temp.z < 150f)) || ((140f < temp.x || temp.z > 130f) || (10f > temp.x || temp.z < 10f))) {
            Born();
        }else
        {
            Controller = temp;
        }
    }

    private void SetCameraLocation()
    {
        var lookDirection = target.transform.position - Controller;
        direction1 = lookDirection;
        lookDirection.y = 0;
        cam.transform.position = Controller - lookDirection.normalized * 45 + Vector3.up * 15;
        cam.transform.rotation = Quaternion.LookRotation(lookDirection.normalized, Vector3.up);
        transform.rotation = Quaternion.LookRotation(lookDirection, Vector3.up);
    }
}

