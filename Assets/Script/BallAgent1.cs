﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class BallAgent1 : Agent
{
    //Kullanacağımız değişkenleri
    public GameObject target; //Hedef Objemizi Alıyoruz
    private Rigidbody ballRb; //Topun rigidbodysini alıyoruz
    private Vector3 throwForce,Controller,temp,direction1,distance,_lastPosition; //Top için ortam içerisinde kullanılacak ^boyutlu vektör değişkenni tanımlıyoruz
    private bool _ballPos = false; //Ajanın verdiği kararların dğru olup olmadığını kontrol eden değişkenlerimiz
    public ScoreArea ScoreArea; //Ajanın score alıp alamadığını kontrol için yazdığımız scripti çağırıyoruz
    public RewardTriger RewardTriger,RewardTriger1;//Deep Rainforcement Learning Algoritmasının çalışma mantığı olan Reward Signalleri için ajanın verdiği kararlar doğrultusunda ödül kazanacağı mekanizma değişkenleri
    public collision collision,collision1,collision2,collision3,collision4,collision5; // Ajanın verdiği kararları yönlendirmek için kullandığımız scriptleri çağırıyoruz.
    private int step,Score=0; //Null
    [SerializeField]
    private Camera cam; // Ajanımızın yeniden doğması sonucunda kamera ajanımızın pozisyonu doğrultusunda hareket ediyor.

    //Ajanımız çalışmaya başladığı zaman aktif olacak etkenler tanımlanır.
    public override void InitializeAgent()
    {
        ballRb = GetComponent<Rigidbody>();
    }

    //Ajanın karar vermesine yarayan Desicion() fonksiyonu her çağrıldığında ajanımız 
    //Ortam içerisinde belirtilen nesnelerin verilerini toplar
    public override void CollectObservations()
    {
        distance = target.transform.position - transform.position;
        distance.y = 0;
        AddVectorObs(distance.magnitude);
    }

    //Her saniye oyun içerisinde güncelleme yapılmasını sağlayan fonksiyon
    private void FixedUpdate()
    {
        //Ajanın verdiği karar doğrultusunda ajan ortam içerisinde herhangi bir duvara çarparsa negatif ödül alır
        if(collision.exit || collision1.exit || collision2.exit || collision3.exit || collision4.exit || collision5.exit){
            AddReward(-1f);
            Done();
        }
        else
        {
            //Ajanın verdiği karar doğrultusunda ajan pota üzerine çıkarsa pozitif ödül ile ödüllendirilir
            if (transform.position.y > 5.5f)
            {
                _ballPos = true;
                AddReward(0.01f);
                if(RewardTriger.rewardCheck){
                    AddReward(0.1f);
                }
                if (RewardTriger1.rewardCheck)
                {
                    AddReward(0.1f);
                }
            }
            if (transform.position.y < 5 && _ballPos)
            {
                //Ajanın verdiği karar doğrutusunda atış Skor getirirse ajan pozitif ödüllendirilir.
                if(ScoreArea.bsktScore)
                {
                    Debug.Log(step + "-" + Score++);
                    AddReward(1f);
                    Done();
                }
            }

            //Ajan verdiği karar ile atış pota seviyesinin altında kalırsa ajan negatif ödüllendirilir.

            if(!_ballPos && transform.position.y < _lastPosition.y)
            {
                AddReward(-0.7f);
                Done();
            }
        }

        _lastPosition = transform.position;
    }

    //Ajanın karar verdiği fonksiyon
    public override void AgentAction(float[] vectorAction, string textAction)
	{
        //Ajanın verdiği kararlar PPO doğrultusunda -1/1 aralığındadır.
        throwForce = new Vector3(0f,vectorAction[0],vectorAction[1]);
        //Ajanın 0 dan düşük aldığı kararlar için negatif ödüllendirme kullanıyoruz
        if (vectorAction[0]<0)
        {
            AddReward(-10f);
        }
        if(vectorAction[1]<0)
        {
            AddReward(-10f);
        }
        //Ajanın verdiği karar hesaplanan kuvvet katsayısı ile çarpılır
        ballRb.AddForce(throwForce * 1855);
    }

    //Senaryo her tamamlandığında ajan resetlenir.
    public override void AgentReset()
    {
        //Ajanın pozisyonu tekrar ayarlanır
        Born();
        transform.position = Controller;
        //Kamera pozisyonu ajanın bulunduğu lokasyona göre ayarlanır.
        SetCameraLocation();

        //Ajanın hızı sıfırlanır
        ballRb.velocity = Vector3.zero;
        ballRb.angularVelocity = Vector3.zero;

        //Kontrol mekanizmalarımız sıfırlanır
        ScoreArea.bsktScore = false;
        RewardTriger.rewardCheck = false;
        RewardTriger1.rewardCheck = false;
        _ballPos=false;
        _lastPosition = Controller;
        //Ajanın karar vermesi için ortam değişkenlerini toplaması istenir.
        RequestDecision();
        step++;

    }

    private void Born()
    {
        temp = new Vector3(Random.value*500/3,5,Random.value*500/3);

        // if(((50f < temp.x && temp.z > 89f) && (99f > temp.x && temp.z < 150f)) || ((140f < temp.x || temp.z > 130f) || (10f > temp.x || temp.z < 10f))) {
        //     Born();
        // }else
        // {
        //     Controller = temp;
        // }
        //Random.Range(-7.2f,1.2f)
        Controller = new Vector3(0f,1f,Random.Range(-7.2f,0f));
        collision.exit=false;
        collision1.exit=false;
        collision2.exit=false;
        collision3.exit=false;
        collision4.exit=false;
        collision5.exit=false;
    }

    private void SetCameraLocation()
    {
        var lookDirection = target.transform.position - Controller;
        direction1 = lookDirection;
        lookDirection.y = 0;
        cam.transform.position = Controller - lookDirection.normalized * 5.5f + Vector3.up * 1.5f;
        cam.transform.rotation = Quaternion.LookRotation(lookDirection.normalized, Vector3.up);
        transform.rotation = Quaternion.LookRotation(lookDirection, Vector3.up);
    }
}
