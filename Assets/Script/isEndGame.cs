﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isEndGame : MonoBehaviour
{
    public bool isEndOfGame = false;
    private void OnCollisionEnter(Collision other) {
        var ballCheck = GetComponent<baller>();
        if (ballCheck.endCheck)
        {
            isEndOfGame = true;
            Debug.Log("End Game");
        }
    }
}
