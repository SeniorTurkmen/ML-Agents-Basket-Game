﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCulling : MonoBehaviour
{
    [SerializeField]
    private Transform _cameraTransform;

    private void Update()
    {
        var wallToCameraDirection = (_cameraTransform.position - transform.position).normalized;

        var result = Vector3.Dot(transform.forward, wallToCameraDirection);
        var meshRenderer = GetComponent<MeshRenderer>();
        var grafity = gameObject.GetComponentInChildren<MeshRenderer>();

        if(meshRenderer != null && grafity != null)
        {
            meshRenderer.enabled = result > 0;
            if (!meshRenderer.enabled)
            {
                grafity.enabled = false;
            }
        }
    }
}