﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceTest : MonoBehaviour
{
    public Vector3 Force;
    public Rigidbody _rb;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            _rb.AddForce(Force);
        }
        
        if(Input.GetKeyDown(KeyCode.Y))
        {
            _rb.velocity = Vector3.zero;
            _rb.angularVelocity = Vector3.zero;
            transform.position = new Vector3(0,0.5f,0.05f);
        }
    }
}
