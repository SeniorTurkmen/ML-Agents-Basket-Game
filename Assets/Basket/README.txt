Basketball Arcade

A Complete project by NitroGames (Martin Gonzalez Developer)(NG)

Include:
-Menu (Main Menu-Options-Select Team);
-FBX's (Complete Scene - Arms rigged and animated);
-Textures and Lightmapping;
-Edited Sounds;
-Fonts;
-Scripts;

It's an Arcade game. Select your Team and Try your best shoots.

Controls:
Forward = W
Backward = S
Left = A
Right = D
Jump = Spacebar
Shoot = Left Click
Options = O

TIPS:

2x = 2 consecutive goals (Impact in your final score)
3x = 3 consecutive goals (Impact in your final score) and you get a fireball

The game ends when you throw all the balls (20 balls) or the timer gets to 0.

Hope you enjoy this project! 

for any support,simply contact me via

gonzalez.martin90@gmail.com


Martin Gonzalez